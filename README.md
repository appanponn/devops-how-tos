This repository contains demo code, thoughts & write-ups on *software/systems dev processes* and how to measure & improve them. Specific topics that include are:
* Continuous Integration (CI) / Delivery (CD)
* DevOps & GitOps
* Infransructure-as-Code (IaC): specifically Ansible
* Value Stream Mapping (VSM)

## Tasks & their Status

- [ ] Check if the `scripts` tags in `package.json` support the prefix convention, was not aware of it
- [ ] What are the *four principles of Continuous Delivery* identified in the [Jez Humble's book on CD](https://www.informit.com/store/continuous-delivery-reliable-software-releases-through-9780321601919)
    * [Four principles of low-risk software releases](https://www.informit.com/articles/article.aspx?p=1833567)
    * Principle 1: Low-Risk Releases Are Incremental
    * Principle 2: Decouple Deployment and Release
    * Principle 3: Focus on Reducing Batch Size
    * Principle 4: Optimize for Resilience
- [ ] What are the *five practices of Continuous Delivery* identified in CD book?
    * [Analysis for CD: Five Core Practices](Analysis for Continuous Delivery: Five Core Practices)
    * Practice 1: Start with a minimum viable product (MVP).
    * Practice 2: Measure the value of your features.
        * [Validated learning](http://blogs.hbr.org/cs/2010/02/entrepreneurs_beware_of_vanity_metrics.html)
    * Practice 3: Perform just enough analysis up front.
        * Before development starts on a story, only care about *three things*:
            * What is *Marginal value of delivering* the story?
            * What is *Marginal cost of deliverying* a story?
            * Do we have enough info to *begin* delivering a story?
        * You need at least [one acceptance criterion](http://dannorth.net/whats-in-a-story/)
    * Practice 4: Do less.
        * [Bill Wake's INVEST principle](http://xp123.com/articles/invest-in-good-stories-and-smart-tasks/) - Independent, Negotiable, Valuable, Estimable, Small & Testable.
        * Summary of a story should fit a 3x5 inex card, *not taking more than a couple of days* to complete.
        * According to [this study]()., more than *half of all functionality developed is never or rarely used*.
    * Practice 5: Include feature toggles in your stories.
        * Turn off a problematic feature when a release goes wrong, as an alternative to rolling back your deployment.
        * Isolates deployment & release.
- [ ] How are these practices related to which principles?
- [ ] Create playbook for GoCD setup in Linode VM instance
    * Setup the instance with the Analytics plugin which requires PostgreSQL
- [ ] Setup Azure AD as the authentication source for GoCD.
    * GoCD [LDAP/AD Auth Plugin - Examples](https://extensions-docs.gocd.org/ldap/current/examples/)
    * [Tutorial: Configure secure LDAP for an Azure Active Directory Domain Services managed domain](https://docs.microsoft.com/en-us/azure/active-directory-domain-services/tutorial-configure-ldaps#lock-down-secure-ldap-access-over-the-internet)
- [ ] How can we track & measure time spent on *fuzzy front-end* of release pipeline:
    * For a feature: *Fuzzy front-end* includes analysis/design, coding & dev testing.
    * For a bug-fix: *Fuzzy front-end* includes finding source of bug, fixing it in code & dev testing.
    * Most teams create bugfix or feature branches for working on them with branch name matching bug or feature ID.
    * Git supports [branch metadata](https://stackoverflow.com/questions/11886132/can-i-add-a-message-note-comment-when-creating-a-new-branch-in-git) which can also be used if it is used by dev team.
 
## References

1. [Continuous Integration, Delivery and Deployment: A Systematic Review on Approaches, Tools, Challenges and Practices](https://arxiv.org/abs/1703.07019), Mar 2017

